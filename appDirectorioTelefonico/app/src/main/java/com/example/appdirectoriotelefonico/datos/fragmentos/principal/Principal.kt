package com.example.appdirectoriotelefonico.datos.fragmentos.principal

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appdirectoriotelefonico.R
import com.example.appdirectoriotelefonico.datos.ContactosVM
import com.example.appdirectoriotelefonico.datos.ListaContactos
import kotlinx.android.synthetic.main.fragment_principal.*
import kotlinx.android.synthetic.main.fragment_principal.view.*

class Principal : Fragment() {

    private lateinit var mContactosVM: ContactosVM

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_principal, container, false)

        val adapter = ListaContactos()
        val recyclerview = view.recyclerview
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(requireContext())

        mContactosVM = ViewModelProvider(this).get(ContactosVM::class.java)
        mContactosVM.readAllData.observe(viewLifecycleOwner, Observer { contacto ->
            adapter.setData(contacto)
        })

        view.floatingActionButton.setOnClickListener() {
            findNavController().navigate(R.id.action_principal_to_agregar)
        }

        setHasOptionsMenu(true)

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_borrar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.borrarMenu){
            deleteAllContactos()
        }
        return super.onContextItemSelected(item)
    }

    private fun deleteAllContactos() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Sí"){_, _ ->
            mContactosVM.deleteAllContactos()
            Toast.makeText(
                requireContext(),
                "Contactos eliminados correctamente",
                Toast.LENGTH_SHORT).show()
        }
        builder.setNegativeButton("No"){_, _ -> }
        builder.setTitle("¿Eliminar todos tus contactos?")
        builder.setMessage("¿Estás seguro de eliminar todos tus contactos?")
        builder.create().show()
    }

}