package com.example.appdirectoriotelefonico.datos

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Contacto::class], version = 1, exportSchema = false)
abstract class DBContactos:RoomDatabase() {

    abstract fun contactos(): Contactos

    companion object{
        @Volatile
        private var INSTANCE: DBContactos? = null

        fun getDatabase(context: Context): DBContactos{
            val tempInsatnce = INSTANCE
            if(tempInsatnce != null){
                return tempInsatnce
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DBContactos::class.java,
                    "db_contacto"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}