package com.example.appdirectoriotelefonico

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.appdirectoriotelefonico.R.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        setupActionBarWithNavController(findNavController(R.id.FragmentContainerView))
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.FragmentContainerView)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}