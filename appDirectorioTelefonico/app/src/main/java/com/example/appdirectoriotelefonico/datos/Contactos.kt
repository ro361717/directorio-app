package com.example.appdirectoriotelefonico.datos

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface Contactos {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addContacto(contacto: Contacto)

    @Update
    suspend fun updateContacto(contacto: Contacto)

    @Delete
    suspend fun deleteContacto(contacto: Contacto)

    @Query("DELETE FROM t_Contactos")
    suspend fun deleteAllContacts()

    @Query("SELECT * FROM t_Contactos ORDER BY nombre ASC")
    fun readAllData(): LiveData<List<Contacto>>
}