package com.example.appdirectoriotelefonico.datos.fragmentos.agregar

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.appdirectoriotelefonico.R
import com.example.appdirectoriotelefonico.datos.Contacto
import com.example.appdirectoriotelefonico.datos.ContactosVM
import kotlinx.android.synthetic.main.fragment_agregar.*
import kotlinx.android.synthetic.main.fragment_agregar.view.*

class agregar : Fragment() {

    private lateinit var mContactosVM: ContactosVM

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_agregar, container, false)

        mContactosVM = ViewModelProvider(this).get(ContactosVM::class.java)

        view.button.setOnClickListener{
            insertDataToDatabase()
        }

        return view
    }

    private fun insertDataToDatabase() {
        val nombre = editTextTextPersonName.text.toString()
        val apellido = editTextTextPersonName2.text.toString()
        val numtel = editTextTextPersonName3.text
        val correo = editTextTextPersonName4.text.toString()

        if(inputCheck(nombre, apellido, numtel, correo)){
            val contacto = Contacto(0, nombre, apellido, Integer.parseInt(numtel.toString()), correo)
            mContactosVM.addContacto((contacto))
            Toast.makeText(requireContext(), "Contacto añadido", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_agregar_to_principal)
        } else{
            Toast.makeText(requireContext(), "Ingresa todos los datos", Toast.LENGTH_LONG).show()
        }
    }
    private fun inputCheck(nombre: String, apellido: String, numtel: Editable, correo: String): Boolean{
        return !(TextUtils.isEmpty(nombre) && TextUtils.isEmpty(apellido) && numtel.isEmpty() && TextUtils.isEmpty(correo))
    }

}