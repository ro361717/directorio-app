package com.example.appdirectoriotelefonico.datos

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactosVM(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<Contacto>>
    private val repository: RepContactos

    init{
        val contactos = DBContactos.getDatabase(application).contactos()
        repository = RepContactos(contactos)
        readAllData = repository.readAllData
    }

    fun addContacto(contacto: Contacto){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addContacto(contacto)
        }
    }
    fun updateContacto(contacto: Contacto){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateContacto(contacto)
        }
    }

    fun deleteContacto(contacto: Contacto){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteContacto(contacto)
        }
    }

    fun deleteAllContactos(){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllContactos()
        }
    }

}