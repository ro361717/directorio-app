package com.example.appdirectoriotelefonico.datos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.appdirectoriotelefonico.R
import com.example.appdirectoriotelefonico.datos.fragmentos.principal.PrincipalDirections
import kotlinx.android.synthetic.main.lista_contacos.view.*

class ListaContactos: RecyclerView.Adapter<ListaContactos.MyViewHolder>(){

    private var contactoLista = emptyList<Contacto>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.lista_contacos, parent, false))
    }

    override fun getItemCount(): Int {
        return contactoLista.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = contactoLista[position]
        holder.itemView.textView.text = currentItem.id.toString()
        holder.itemView.textView2.text = currentItem.nombre
        holder.itemView.textView3.text = currentItem.apellido
        holder.itemView.textView5.text = currentItem.numtel.toString()
        holder.itemView.textView6.text = currentItem.correo

        holder.itemView.rowLayout.setOnClickListener{
            val action = PrincipalDirections.actionPrincipalToActualizaDatos2(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }
    fun setData(contacto: List<Contacto>){
        this.contactoLista = contacto
        notifyDataSetChanged()
    }
}