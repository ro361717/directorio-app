package com.example.appdirectoriotelefonico.datos

import androidx.lifecycle.LiveData

class RepContactos(private val contactos: Contactos) {

    val readAllData: LiveData<List<Contacto>> = contactos.readAllData()

    suspend fun addContacto(contacto: Contacto){
        contactos.addContacto(contacto)
    }

    suspend fun updateContacto(contacto: Contacto){
        contactos.updateContacto(contacto)
    }

    suspend fun deleteContacto(contacto: Contacto){
        contactos.deleteContacto(contacto)
    }

    suspend fun deleteAllContactos() {
       contactos.deleteAllContacts()
    }
}