package com.example.appdirectoriotelefonico.datos.fragmentos.actulizar

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.*
import android.view.accessibility.AccessibilityManager
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.appdirectoriotelefonico.R
import com.example.appdirectoriotelefonico.datos.Contacto
import com.example.appdirectoriotelefonico.datos.ContactosVM
import kotlinx.android.synthetic.main.fragment_actualiza_datos.*
import kotlinx.android.synthetic.main.fragment_actualiza_datos.view.*


class  actualizaDatos : Fragment() {

    private val args by navArgs<actualizaDatosArgs>()

    private lateinit var mContactosVM: ContactosVM

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_actualiza_datos, container, false)

        mContactosVM = ViewModelProvider(this).get(ContactosVM::class.java)

        view.editTextTextPersonName5.setText(args.contactoActual.nombre)
        view.editTextTextPersonName6.setText(args.contactoActual.apellido)
        view.editTextTextPersonName7.setText(args.contactoActual.numtel.toString())
        view.editTextTextPersonName8.setText(args.contactoActual.correo)

        view.button2.setOnClickListener{
            updateItem()
        }

        setHasOptionsMenu(true)

        return view
    }
    private fun updateItem(){
        val nombre = editTextTextPersonName5.text.toString()
        val apellido = editTextTextPersonName6.text.toString()
        val numt = Integer.parseInt(editTextTextPersonName7.text.toString())
        val correo = editTextTextPersonName8.text.toString()

        if(inputCheck(nombre, apellido, editTextTextPersonName7.text, correo)){

            val updateContacto = Contacto(args.contactoActual.id, nombre, apellido, numt, correo)

            mContactosVM.updateContacto(updateContacto)
            Toast.makeText(requireContext(), "Contacto modificado", Toast.LENGTH_SHORT).show()

            findNavController().navigate(R.id.action_actualizaDatos2_to_principal)
        } else{
            Toast.makeText(requireContext(), "Agrega todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(nombre: String, apellido: String, numtel: Editable, correo: String): Boolean{
        return !(TextUtils.isEmpty(nombre) && TextUtils.isEmpty(apellido) && numtel.isEmpty() && TextUtils.isEmpty(correo))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_borrar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.itemId == R.id.borrarMenu){
            deleteContacto()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun deleteContacto() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Sí") { _, _ ->
            mContactosVM.deleteContacto(args.contactoActual)
            Toast.makeText(
                requireContext(),
                "${args.contactoActual.nombre} ha sido eliminado de tus contactos",
                Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_actualizaDatos2_to_principal)
        }
        builder.setNegativeButton("No") { _, _ -> }
        builder.setTitle("Eliminar a ${args.contactoActual.nombre}")
        builder.setMessage("¿Estás seguro de eliminar a ${args.contactoActual.nombre} de tus contactos?")
        builder.create().show()
    }
}