package com.example.appdirectoriotelefonico.datos

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "t_Contactos")
data class Contacto(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val nombre: String,
    val apellido: String,
    val numtel: Int,
    val correo: String
): Parcelable